

#include "FaceActivity.h"

FaceActivity::FaceActivity() {
  modeChanged = true;
}

void FaceActivity::handle() {
  handleLeftEye();
  handleRightEye();
  handleSingles();
  handleMouth();
  checkMode();
}

void FaceActivity::init(){
  this->mouth->Fade(this->mouth->Color(255, 0,0), this->mouth->Color(255, 0,255), 25, 100);
  this->singles->RainbowCycle(5);
}

void FaceActivity::handleLeftEye(){
  if (this->leftEyeButton->isActive()) {
    if(modeChanged){
      switch (mode) {
        case 0:
          Serial.println("Colorwipe");
          this->leftEye->ColorWipe(this->leftEye->Color(255, 255,0), 100);
          break;
        case 1:
          Serial.println("rainbow");
         this->leftEye->RainbowCycle( 5);
          break;
        case 2:
          Serial.println("theatercha");
          this->leftEye->TheaterChase(this->leftEye->Color(255, 0,0), this->leftEye->Color(0, 0,255), 100);
          break;
        case 3:
          Serial.println("scanner");
          this->leftEye->Scanner(this->leftEye->Color(255, 0,0), 100);
          break;
        default:
          Serial.println("Fade");
          this->leftEye->Fade(this->leftEye->Color(255, 0,0), this->leftEye->Color(0, 0,255), 25, 100);
          break;
      }

    }
  }
}

void FaceActivity::handleRightEye(){
  if (this->rightEyeButton->isActive()) {
    if(modeChanged){
      switch (mode) {
        case 2:
          this->rightEye->ColorWipe(this->rightEye->Color(255, 0,0), 100);
          break;
        case 1:
          this->rightEye->RainbowCycle( 5);
          break;
        case 3:
          this->rightEye->TheaterChase(this->rightEye->Color(255, 0,0), this->rightEye->Color(0, 0,255), 100);
          break;
        case 0:
          this->rightEye->Scanner(this->rightEye->Color(255, 0,0), 100);
          break;
        default:
          this->rightEye->Fade(this->rightEye->Color(255, 0,0), this->rightEye->Color(255, 0,255), 25, 100);
          break;
      }

    }
  } 
}


void FaceActivity::handleSingles(){
  if (this->rightEarButton->isActive()) {
    if(modeChanged){
      switch (mode) {
        case 3:
        this->singles->ColorWipe(this->leftEye->Color(255, 0,0), 100);
          break;
        case 2:
        this->singles->RainbowCycle( 5);
          break;
        case 0:
        this->singles->TheaterChase(this->leftEye->Color(255, 0,0), this->leftEye->Color(0, 0,255), 100);
          break;
        case 1:
        this->singles->Scanner(this->leftEye->Color(255, 0,0), 100);
          break;
        default:
        this->singles->Fade(this->leftEye->Color(255, 0,0), this->leftEye->Color(255, 0,255), 25, 100);
          break;
      }
    }
  } 
}


void FaceActivity::handleMouth(){
  if (this->leftEarButton->isActive()) {
    if(modeChanged){
      switch (mode) {
        case 2:
        this->mouth->ColorWipe(this->leftEye->Color(255, 0,0), 100);
          break;
        case 3:
        this->mouth->RainbowCycle( 5);
          break;
        case 0:
        this->mouth->TheaterChase(this->leftEye->Color(255, 0,0), this->leftEye->Color(0, 0,255), 100);
          break;
        case 1:
        this->mouth->Scanner(this->leftEye->Color(255, 0,0), 100);
          break;
        default:
        this->mouth->Fade(this->leftEye->Color(255, 0,0), this->leftEye->Color(255, 0,255), 25, 100);
          break;
      }
    }
  } 
}


String FaceActivity::print() {
  return "FaceActivity print";
}

bool FaceActivity::shouldActivate() {
  return true;
}

void FaceActivity::checkMode() {
  if(this->noseButton->isActive() && this->noseButton->isChanged()){
      mode = (mode > MAX_MODES ? 0 : ++mode);
      modeChanged = true;
      Serial.println("mode change");

  }else{
    modeChanged = false;
  }
}