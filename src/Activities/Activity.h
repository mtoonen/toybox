#ifndef ACTIVITY_H
#define ACTIVITY_H
#include <Arduino.h>
#include "../config.h"
#include "../Button/Button.h"
#include "NeoPatterns/NeoPatterns.h"

class Activity{
    public:
        Activity();
        virtual void handle() =0;
        virtual bool shouldActivate() =0;
        virtual String print() =0;
        void preHandle();
        static void leftEyeComplete();
        static void rightEyeComplete();
        static void mountComplete();
        static void singlesComplete();

        virtual void init() =0;

    protected:
        NeoPatterns *leftEye;
        NeoPatterns *rightEye;

        NeoPatterns* singles ;
        NeoPatterns* mouth ;

        Button* leftEyeButton;
        Button* rightEyeButton;

        Button* leftEarButton;
        Button* rightEarButton;

        Button* noseButton;
        
};

#endif