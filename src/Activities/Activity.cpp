#include "Activity.h"

Activity::Activity(){
  this->leftEyeButton = new Button(LEFT_EYE_BUTTON_PIN, LEFT_EYE_BUTTON_LED_PIN);
  this->rightEyeButton = new Button(RIGHT_EYE_BUTTON_PIN, RIGHT_EYE_BUTTON_LED_PIN);

  this->leftEarButton = new Button(LEFT_EAR_BUTTON_PIN, LEFT_EAR_BUTTON_LED_PIN);
  this->rightEarButton = new Button(RIGHT_EAR_BUTTON_PIN, RIGHT_EAR_BUTTON_LED_PIN);

  this->noseButton = new Button(NOSE_BUTTON_PIN, NOSE_BUTTON_LED_PIN);

  this->leftEye = new NeoPatterns(LEFT_EYE_PIXELS, LEFT_EYE_DATA_PIN, NEO_RGBW + NEO_KHZ800, this->leftEyeComplete);
  this->leftEye->setBrightness(BRIGHTNESS_EYES);
  
  this->rightEye = new NeoPatterns(RIGHT_EYE_PIXELS, RIGHT_EYE_DATA_PIN, NEO_RGBW + NEO_KHZ800, this->rightEyeComplete);
  this->rightEye->setBrightness(BRIGHTNESS_EYES);

  this->singles= new NeoPatterns(SINGLES_PIXELS, SINGLES_DATA_PIN, NEO_GRB + NEO_KHZ800, this->singlesComplete);
  this->singles->setBrightness(BRIGHTNESS_SINGLES);

  this->mouth= new NeoPatterns(MOUTH_PIXELS, MOUTH_DATA_PIN, NEO_RGBW + NEO_KHZ800, this->mountComplete);
  this->mouth->setBrightness(BRIGHTNESS_MOUTH);
}

void Activity::preHandle(){
    this->leftEyeButton->check();
    this->rightEyeButton->check();
    this->leftEarButton->check();
    this->rightEarButton->check();
    this->noseButton->check();

    if(this->leftEyeButton->isActive()){
      this->leftEye->Update();
    }else{
      this->leftEye->clear();
      this->leftEye->show();
    }
    
    if(this->rightEyeButton->isActive()){
      this->rightEye->Update();
    }else{
      this->rightEye->clear();
      this->rightEye->show();
    }

    if(this->rightEarButton->isActive()){
      this->singles->Update();
    }else{
      this->singles->clear();
      this->singles->show();
    }

    if(this->leftEarButton->isActive()){
      this->mouth->Update();
    }else{
      this->mouth->clear();
      this->mouth->show();
    }
}

void Activity::leftEyeComplete(){
  //Serial.println("leftEyeComplete complete");
}

void Activity::rightEyeComplete(){
  Serial.println("rightEyeComplete complete");
}

void Activity::mountComplete(){
  Serial.println("mountComplete complete");
}


void Activity::singlesComplete(){
  Serial.println("singlesComplete complete");
}
