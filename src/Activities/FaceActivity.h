#ifndef FACE_ACTIVITY_H
#define FACE_ACTIVITY_H
#include "Activity.h"
#include "../Button/Button.h"


class FaceActivity : public Activity{
    public:
        FaceActivity();
        String print();
        bool shouldActivate();
        void handle();
        static void Ring1Complete();
        void init();

    private:
        
        int mode = 2;

        int MAX_MODES = 4;
        bool modeChanged = false;

        void checkMode();
        void handleLeftEye();
        void handleRightEye();
        void handleSingles();
        void handleMouth();

};
#endif