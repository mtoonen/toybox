#ifndef DEBUG_ACTIVITY_H
#define DEBUG_ACTIVITY_H
#include "Activity.h"

class DebugActivity : public Activity{
    public:
        DebugActivity();
        String print();
        bool shouldActivate();
        void handle();
        void init();

    protected:
        int numPixels = 12;

        int left_button_pin = 5;
        
        int DELAYVAL = 10;
        NeoPatterns *left_eye;

        void rainbow();

};
#endif