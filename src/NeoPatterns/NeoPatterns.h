#ifndef NEOPATTERNS_H
#define NEOPATTERNS_H
#include <Adafruit_NeoPixel.h>

enum pattern
{
  NONE,
  RAINBOW_CYCLE,
  THEATER_CHASE,
  COLOR_WIPE,
  SCANNER,
  FADE
};
// Patern directions supported:
enum direction
{
  FORWARD,
  REVERSE
};

// NeoPattern Class - derived from the Adafruit_NeoPixel class
class NeoPatterns : public Adafruit_NeoPixel
{
  public:

        // Constructor - calls base-class constructor to initialize strip
    NeoPatterns(uint16_t pixels, uint8_t pin, uint8_t type, void (*callback)());
    void Update();
    void Reverse();
    void RainbowCycle(uint8_t interval, direction dir = FORWARD);
    void TheaterChase(uint32_t color1, uint32_t color2, uint8_t interval, direction dir = FORWARD);
    void ColorWipe(uint32_t color, uint8_t interval, direction dir = FORWARD);
    void Scanner(uint32_t color1, uint8_t interval);
    void Fade(uint32_t color1, uint32_t color2, uint16_t steps, uint8_t interval, direction dir = FORWARD);
    uint32_t DimColor(uint32_t color);
    void ColorSet(uint32_t color);
    uint8_t Red(uint32_t color);
    uint8_t Green(uint32_t color);
    uint8_t Blue(uint32_t color);
    uint32_t Wheel(byte WheelPos);

  private:
    // Member Variables:
    pattern ActivePattern; // which pattern is running
    direction Direction;   // direction to run the pattern

    unsigned long Interval;   // milliseconds between updates
    unsigned long lastUpdate; // last update of position

    uint32_t Color1, Color2; // What colors are in use
    uint16_t TotalSteps;     // total number of steps in the pattern
    uint16_t Index;          // current step within the pattern
    
    void (*OnComplete)(); // Callback
    void Increment();
    void RainbowCycleUpdate();
    void FadeUpdate();
    void ScannerUpdate();
    void ColorWipeUpdate();
    void TheaterChaseUpdate();

};
#endif