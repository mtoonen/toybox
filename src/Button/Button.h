#ifndef BUTTON_H
#define BUTTON_H
#include <Arduino.h>
#include "../config.h"

class Button{
    public:
        Button(int inputPin, int ledPin);
        void check();
        bool isActive();
        bool isChanged();
        void on();
        void off();
        int getVal();

    private:
        int inputPin;
        int ledPin;
        bool currentState = false;
        bool isButtonChanged = false;
    
};

#endif