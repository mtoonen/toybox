#include "Button.h"

Button::Button(int inputPin, int ledPin)
{
    this->inputPin = inputPin;
    this->ledPin = ledPin;
    pinMode(this->inputPin, INPUT_PULLDOWN);
    pinMode(this->ledPin, OUTPUT);
}

void Button::check()
{
    if(debug){
       // Serial.printf("Button with pin %d checked\n",pin);
       
    }
    int stateButton = digitalRead(this->inputPin);
    if (stateButton == 1)
    {
        if (currentState == false)
        {
            isButtonChanged = true;
        }
        else
        {
            isButtonChanged = false;
        }
        currentState = true;
        digitalWrite(ledPin, 255);
    }
    else
    {
        if (currentState == true)
        {
            isButtonChanged = true;
        }
        else
        {
            isButtonChanged = false;
        }
        currentState = false;
        digitalWrite(ledPin, 0);
    }
}

bool Button::isActive()
{
    return currentState;
}

bool Button::isChanged()
{
    return isButtonChanged;
}

void Button::on(){
        digitalWrite(ledPin, 255);

}
void Button::off(){
        digitalWrite(ledPin, 0);

}

int Button::getVal(){
    int stateButton = digitalRead(this->inputPin);
    return stateButton;

}