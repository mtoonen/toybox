#ifndef UPDATER_h
#define UPDATER_h
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include "../config.h"


class Updater
{
public:
    Updater();
    void init();
    void check();

private:
    double ratio = 100.0;
};
#endif