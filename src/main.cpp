#include "LittleFS.h"
#include "config.h"
#include "Updater/Updater.h"
#include "Activities/Activity.h"
#include "Activities/DebugActivity.h"
#include "Activities/FaceActivity.h"

#define LED  2
#define INPUT 4

Updater updater;
Activity* current = NULL;
Activity* activities[2];

bool updaterInit = false;

void chooseActivity(){
  Activity* temp = NULL;
  for( Activity* acc : activities ){
    if(acc->shouldActivate()){
      temp = acc;
      break;
    }
  }

  if(temp == NULL){
    Serial.println("No activity found, defaulting");
    temp = new DebugActivity();
  }
  current = temp;
}

void updaterRoutine(){

  int valLeft = digitalRead(LEFT_EAR_BUTTON_PIN);
  int valRight = digitalRead(LEFT_EAR_BUTTON_PIN);
  int nose = digitalRead(NOSE_BUTTON_PIN);
  if(valLeft == 1 && valRight == 1 && nose == 1){
    delay(5000);
    Serial.println("Update sequence started");
    
    updaterInit = updater.init();
    if(updaterInit){
     updater.check();
    }
  }

}


void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  pinMode(INPUT, INPUT);

  Serial.println("Booting");
  current = new DebugActivity();
  activities [0] = current;
  activities [1] = new FaceActivity();
  chooseActivity();
  Serial.println("Ready");
  current->init();
  //updaterRoutine();

}
void loop() {
  // chooseActivity();
  if(current != NULL){
    current->preHandle();
    current->handle();
    
  }
}

